from ast import Str
from glob import glob
from random import *
from tokenize import Name
from turtle import update
from unicodedata import name
from urllib.request import Request
from wsgiref.util import request_uri
from xmlrpc.client import Boolean
from zipapp import create_archive
import mariadb
import sys
import json
from numpy import append, array, nbytes, true_divide
from flask import Flask, render_template, request, url_for, redirect


# Global Variables:
app = Flask(__name__)
cursor = None
webGameDB = None
UserList = []
GameList = []
GameidList = []

# connect to DB:


def getConnection():
    global cursor
    global webGameDB
    # Connect to MariaDB Platform
    try:
        webGameDB = mariadb.connect(
            user="server2",
            password="server2",
            host="127.0.0.1",
            port=3306,
            database="webGameDB"

        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)

    # Get Cursor
    cursor = webGameDB.cursor()
# Add User (object->JSON) to DB Table Users:


def addUserToDB(CurrentUser):
    getConnection()
    jso = json.dumps(CurrentUser.__dict__)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "INSERT INTO Users (userData) VALUES ('{}');".format(jso)
        )
        print("Successfully added user to database")
    except mariadb.Error as e:
        print(f"Error adding user to database: {e}")
    webGameDB.commit()
    CurrentUser.id = cursor.lastrowid
    UpdateDBUser(CurrentUser)
# Add Game (object->JSON) to DB Table Games:


def addGameToDB(CurrentGame):
    getConnection()
    jso = json.dumps(CurrentGame.__dict__)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "INSERT INTO Games (gameData) VALUES ('{}');".format(jso)
        )
        print("Successfully added game to database")
    except mariadb.Error as e:
        print(f"Error adding game to database: {e}")
    webGameDB.commit()
    CurrentGame.DBid = cursor.lastrowid
    UpdateDBGame(CurrentGame)
# Update user Data in DB:


def UpdateDBUser(CurrentUser):
    getConnection()
    jso = json.dumps(CurrentUser.__dict__)
    print(jso)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "update Users set userData = '{}' where id = {};".format(
                jso, CurrentUser.id)
        )
        print("Successfully update entry to database")
    except mariadb.Error as e:
        print(f"Error update entry to database: {e}")
    webGameDB.commit()
    webGameDB.close()

# Update Game Data in DB:


def UpdateDBGame(CurrentGame):
    getConnection()
    jso = json.dumps(CurrentGame.__dict__)
    print(jso)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "update Games set gameData = '{}' where id = {};".format(
                jso, CurrentGame.DBid)
        )
        print("Successfully update Game to database")
    except mariadb.Error as e:
        print(f"Error update Game to database: {e}")
    webGameDB.commit()
    webGameDB.close()
# Get from DB Users list:


def getUserList():
    getConnection()
    global cursor
    global webGameDB
    global UserList
    UserList = []
    try:
        cursor.execute(
            "SELECT userData FROM Users;"
        )
        for userData in cursor:
            jsonData = ','.join(userData)
            j = json.loads(jsonData)
            user = UserDB(**j)
            UserList.append(user)
            print("Successfully load database")
    except mariadb.Error as e:
        print(f"Error adding entry to database: {e}")

    webGameDB.commit()
    webGameDB.close()
# Get from DB Games list:


def getGameList():
    getConnection()
    global cursor
    global webGameDB
    global GameList
    GameList = []
    try:
        cursor.execute(
            "SELECT gameData FROM Games;"
        )
        for gameData in cursor:
            jsonData = ','.join(gameData)
            j = json.loads(jsonData)
            game = GameDB(**j)
            GameList.append(game)
            print("Successfully load Game database")
    except mariadb.Error as e:
        print(f"Error load entry to Game database: {e}")
    webGameDB.commit()
    webGameDB.close()
# Remove User from DB:


def rmUserToDB(user):
    global cursor
    global webGameDB
    getConnection()
    id = user.id
    try:
        cursor.execute(
            "delete from Users where id = {};".format(id)
        )
        print("Successfully rm entry to database")
    except mariadb.Error as e:
        print(f"Error adding entry to database: {e}")
    webGameDB.commit()
    webGameDB.close()
# Remove Game from DB:


def rmGameToDB(game):
    global cursor
    global webGameDB
    getConnection()
    id = game.DBid
    try:
        cursor.execute(
            "delete from Games where id = {};".format(id)
        )
        print("Successfully remove game to database")
    except mariadb.Error as e:
        print(f"Error remove game to database: {e}")
    webGameDB.commit()
    webGameDB.close()


def getIDlist():
    global GameidList
    global GameList
    GameidList = []
    for game in GameList:
        GameidList.append(game.DBid)


class User:
    def __init__(self, Name):
        self.Name = Name
        self.Path = ""
        self.Carte = ""
        self.Tas = []
        self.Defausse = []
        self.Nb_card = 0


class UserDB(object):
    def __init__(self, Name, id, Path, Carte, Tas, Defausse, Nb_card):
        self.id = id
        self.Name = Name
        self.Path = Path
        self.Carte = Carte
        self.Tas = Tas
        self.Defausse = Defausse
        self.Nb_card = Nb_card


class Game:
    def __init__(self):
        tas = []

        def cartes(n, p):
            couleur = ['C', 'D', 'H', 'S']
            valeur = ['2', '3', '4', '5', '6', '7',
                      '8', '9', '10', 'J', 'Q', 'K', 'A']
            jeu, mains = [], []
            for c in couleur:
                for v in valeur:
                    jeu.append("{}{}".format(v, c))
            for i in range(p):
                main = []
                while len(main) < n:
                    main.append(jeu.pop(randrange(len(jeu))))
                mains.append(main)
            return mains

        nb_cartes, nb_joueurs = 3, 2
        jeux = cartes(nb_cartes, nb_joueurs)
        for i in range(nb_joueurs):

            tas.append(jeux[i])
        tas_p1 = tas[0]
        tas_p2 = tas[1]
        defausse_p1 = []
        defausse_p2 = []

        self.nb_manche = 0
        self.DBid = None
        self.Players = []
        self.Winner = ""
        self.P1_card = ""
        self.P2_card = ""
        self.P1_tas = tas_p1
        self.P2_tas = tas_p2
        self.P1_defausse = defausse_p1
        self.P2_defausse = defausse_p2
        self.ToursP1 = False
        self.ToursP2 = False
        self.Exaequo = False
        self.Attendre = False
        self.EndGame = False
        self.Info = ""
        self.nb_card_p1 = len(tas_p1)
        self.nb_card_p2 = len(tas_p2)


class GameDB:
    def __init__(self, nb_manche, DBid, Players, Winner, P1_card, P2_card, P1_tas, P2_tas, P1_defausse, P2_defausse, ToursP1, ToursP2, Exaequo, Attendre, EndGame, Info, nb_card_p1, nb_card_p2):
        self.nb_manche = nb_manche
        self.DBid = DBid
        self.Players = Players
        self.Winner = Winner
        self.P1_card = P1_card
        self.P2_card = P2_card
        self.P1_tas = P1_tas
        self.P2_tas = P2_tas
        self.P1_defausse = P1_defausse
        self.P2_defausse = P2_defausse
        self.ToursP1 = ToursP1
        self.ToursP2 = ToursP2
        self.Exaequo = Exaequo
        self.Attendre = Attendre
        self.EndGame = EndGame
        self.Info = Info
        self.nb_card_p1 = nb_card_p1
        self.nb_card_p2 = nb_card_p2

    def rmGame(self, player):
        if len(self.Players) == 1:
            rmGameToDB(self)
            GameidList.remove(self.DBid)
            self.Players.remove(player)
            UpdateDBGame(self)
            print("Succes Remove Game.")
        else:
            self.Players.remove(player)
            UpdateDBGame(self)
            print("Can not remove, there is still one player in the game.")


def getCurrentUser(name=None):
    for user in UserList:
        if user.Name == name:
            return user


def getCurrentGame(idGame=None):
    for game in GameList:
        if str(game.DBid) == str(idGame):
            return game


@app.route('/user/<name>/partie/<idPartie>/endGame', methods=['POST', 'GET'])
def endGame(name=None, idPartie=None):
    currentUser = getCurrentUser(name)
    print(currentUser)
    print(currentUser.Name)
    currentGame = getCurrentGame(idPartie)
    winner = currentGame.Winner
    print(winner)
    nbManche = currentGame.nb_manche
    print(nbManche)
    global GameidList
    global GameList
    global UserList

    if request.method == 'POST':
        if request.form['submit'] == 'Restart':
            GameDB.rmGame(currentGame, currentUser.Name)
            return redirect(url_for('prepartie', name=currentUser.Name))
        if request.form['submit'] == 'Home page':
            GameDB.rmGame(currentGame, currentUser.Name)
            rmUserToDB(currentUser)
            return redirect(url_for('login'))
    return render_template('endGame.html', winner=winner, nbManche=nbManche, user=currentUser.Name)


# page de la partie
@app.route('/user/<name>/partie/<idPartie>', methods=['POST', 'GET'])
def partie(name=None, idPartie=None):
    getGameList()
    getUserList()
    currentUser = getCurrentUser(name)
    print(currentUser)
    currentGame = getCurrentGame(idPartie)
    print(currentGame)
# variables Affichage
    player1_name = currentUser.Name
    player2_name = "Loading ..."
    player1_Carte = "back"
    player2_Carte = "back"
    player1_Nb_card = 0
    player2_Nb_card = 0

    if len(currentGame.Players) == 2:
        player1 = getCurrentUser(currentGame.Players[0])
        player1_name = player1.Name
        player2 = getCurrentUser(currentGame.Players[1])
        player2_name = player2.Name
        player1.Tas = currentGame.P1_tas
        player2.Tas = currentGame.P2_tas
        player1.Carte = currentGame.P1_card
        player2.Carte = currentGame.P2_card
        player1.Defausse = currentGame.P1_defausse
        player2.Defausse = currentGame.P2_defausse
        player1.Nb_card = currentGame.nb_card_p1
        player2.Nb_card = currentGame.nb_card_p2
        currentGame.Info = ""
        UpdateDBGame(currentGame)
        UpdateDBUser(currentUser)

        values = {"2C": 2, "3C": 3, "4C": 4, "5C": 5, "6C": 6, "7C": 7, "8C": 8, "9C": 9, "10C": 10, "JC": 11, "QC": 12, "KC": 13, "AC": 14,
                  "2D": 2, "3D": 3, "4D": 4, "5D": 5, "6D": 6, "7D": 7, "8D": 8, "9D": 9, "10D": 10, "JD": 11, "QD": 12, "KD": 13, "AD": 14,
                  "2H": 2, "3H": 3, "4H": 4, "5H": 5, "6H": 6, "7H": 7, "8H": 8, "9H": 9, "10H": 10, "JH": 11, "QH": 12, "KH": 13, "AH": 14,
                  "2S": 2, "3S": 3, "4S": 4, "5S": 5, "6S": 6, "7S": 7, "8S": 8, "9S": 9, "10S": 10, "JS": 11, "QS": 12, "KS": 13, "AS": 14}

        print("--------Gameinfo------------")
        print("tas")
        print(player1.Tas)
        print(player2.Tas)
        print("cartes")
        print(player1.Carte)
        print(player2.Carte)
        print("defausse")
        print(player1.Defausse)
        print(player2.Defausse)
        print("nbrCartes")
        print(player1.Nb_card)
        print(player2.Nb_card)

    if request.method == 'POST':
        # si plus ou moins de 2 joueur seul bouton retour fontionne
        if len(currentGame.Players) != 2:
            currentGame.Info = "You must be 2 players to play, wait for another player to join"
            if request.form["submit"] == "Go back":
                print("retour")
                GameDB.rmGame(currentGame, currentUser.Name)

                return redirect(url_for('prepartie', name=currentUser.Name))

        # si deux joueurs:
        if len(currentGame.Players) == 2:

            if request.form['submit'] == 'Play':
                print("show")
                if currentGame.ToursP1 & currentGame.ToursP2:
                    currentGame.Attendre = False
                    currentGame.Info = "Both players have played"

                    # si valeur j1 > j2
                    if values[player1.Carte] > values[player2.Carte]:
                        currentGame.Info = "{} wins".format(player1.Name)

                        # si il y a une defausse
                        if len(player1.Defausse) > 0:
                            for carte in player1.Defausse:
                                player1.Tas.append(carte)
                            player1.Defausse = []

                        player1.Tas.append(player1.Carte)

                        if len(player2.Defausse) > 0:
                            for carte in player2.Defausse:
                                player1.Tas.append(carte)
                            player2.Defausse = []

                        player1.Tas.append(player2.Carte)
                    # si valeur j1 < j2
                    if values[player2.Carte] > values[player1.Carte]:
                        currentGame.Info = "{} wins".format(player2.Name)

                        if len(player2.Defausse) > 0:
                            for carte in player2.Defausse:
                                player2.Tas.append(carte)
                            player2.Defausse = []

                        player2.Tas.append(player2.Carte)
                        if len(player1.Defausse) > 0:
                            for carte in player1.Defausse:
                                player2.Tas.append(carte)
                            player1.Defausse = []

                        player2.Tas.append(player1.Carte)
                    # si egalité
                    if values[player2.Carte] == values[player1.Carte]:
                        if len(player1.Tas) < 4 or len(player2.Tas) < 4:
                            currentGame.Exaequo = True
                        else:
                            currentGame.P1_defausse.append(
                                player1.Carte)
                            currentGame.P1_defausse.append(
                                player1.Tas.pop(0))
                            currentGame.P1_defausse.append(
                                player1.Tas.pop(0))
                            currentGame.P1_defausse.append(
                                player1.Tas.pop(0))

                            currentGame.P2_defausse.append(
                                player2.Carte)
                            currentGame.P2_defausse.append(
                                player2.Tas.pop(0))
                            currentGame.P2_defausse.append(
                                player2.Tas.pop(0))
                            currentGame.P2_defausse.append(
                                player2.Tas.pop(0))

                else:
                    if currentGame.ToursP1 == False:
                        currentGame.Info = "{} has to play".format(
                            player1.Name)
                        currentGame.Attendre = True
                    if currentGame.ToursP2 == False:
                        currentGame.Info = "{} has to play".format(
                            player2.Name)
                        currentGame.Attendre = True

                if len(player1.Tas) <= 0 or len(player2.Tas) <= 0 or currentGame.Exaequo:
                    currentGame.Winner = "{} wins !".format(player2.Name)
                    if len(player2.Tas) == 0:
                        currentGame.Winner = "{} wins !".format(player1.Name)
                    if currentGame.Exaequo:
                        currentGame.Winner = "Exaequo !!"
                    currentGame.EndGame = True

                if currentGame.Attendre == False:
                    # actualise les datas
                    currentGame.P1_tas = player1.Tas
                    currentGame.P2_tas = player2.Tas
                    player1.Carte = ""
                    player2.Carte = ""

                    currentGame.nb_manche += 1
                    player2.Defausse = []
                    player1.Defausse = []
                    currentGame.P1_card = player1.Carte = ""
                    currentGame.P2_card = player2.Carte = ""
                    currentGame.P1_defausse = player1.Defausse
                    currentGame.P2_defausse = player1.Defausse
                    print(player1.Tas)
                    print(player2.Tas)
                    currentGame.ToursP1 = False
                    currentGame.ToursP2 = False

                player1_Carte = player1.Carte
                player2_Carte = player2.Carte
                player1_Nb_card = len(player1.Tas)
                player2_Nb_card = len(player2.Tas)
                UpdateDBGame(currentGame)
                UpdateDBUser(currentUser)
            if request.form['submit'] == 'Show cards':
                print("Play")
                print(request.path)
                if request.path == "/user/"+player1.Name+"/partie/"+str(idPartie):
                    if currentGame.ToursP1 == False:
                        player1.Carte = player1.Tas.pop(0)
                        currentGame.P1_card = player1.Carte
                        currentGame.ToursP1 = True

                    else:
                        currentGame.Info = "You have already played"

                if request.path == "/user/"+player2.Name+"/partie/"+str(idPartie):
                    if currentGame.ToursP2 == False:
                        player2.Carte = player2.Tas.pop(0)
                        currentGame.P2_card = player2.Carte
                        currentGame.ToursP2 = True

                    else:
                        currentGame.Info = "You have already played"
                player1_Carte = player1.Carte
                player2_Carte = player2.Carte
                player1_Nb_card = len(player1.Tas)
                player2_Nb_card = len(player2.Tas)
                UpdateDBGame(currentGame)
                UpdateDBUser(currentUser)

            if request.form['submit'] == 'Leave game':
                print("Leave")
                if request.path == "/user/"+player1.Name+"/partie/"+str(idPartie):
                    currentGame.Winner = "{} wins !".format(player2.Name)
                    currentGame.EndGame = True

                if request.path == "/user/"+player2.Name+"/partie/"+str(idPartie):
                    currentGame.Winner = "{} wins !".format(player1.Name)
                    currentGame.EndGame = True

                UpdateDBGame(currentGame)
                UpdateDBUser(currentUser)
    if currentGame.EndGame == True:
        return redirect(url_for("endGame", name=currentUser.Name, idPartie=currentGame.DBid))
    return render_template('partie.html',
                           name=name, message=currentGame.Info, idPartie=idPartie, player1=player1_name, player2=player2_name, carteJ1="/static/" + player1_Carte + ".png", carteJ2="/static/" + player2_Carte+".png", NB_carteJ1=player1_Nb_card, NB_carteJ2=player2_Nb_card)

# "lobby" création et chemin vers une partie


@app.route('/user/<name>/', methods=['POST', 'GET'])
def prepartie(name=None):
    global GameList
    getUserList()
    getGameList()
    getIDlist()
    currentUser = getCurrentUser(name)
    users = []
    for user in UserList:
        users.append(user.Name)
    if request.method == 'POST':
        if request.form['submit'] == 'Create Game':
            currentGame = Game()
            addGameToDB(currentGame)
            getGameList()
            getIDlist()
            currentGame = getCurrentGame(GameidList[len(GameidList)-1])
            currentGame.Players.append(currentUser.Name)
            UpdateDBGame(currentGame)
            UpdateDBUser(currentUser)
            return redirect(url_for("partie", name=currentUser.Name, idPartie=GameidList[len(GameidList)-1]))
        if request.form['submit'] == 'Join':
            if len(GameidList) > 0:
                id = request.form['comp_select']
                currentGame = getCurrentGame(id)
                currentGame.Players.append(currentUser.Name)
                UpdateDBGame(currentGame)
                UpdateDBUser(currentUser)
                return redirect(url_for("partie", name=currentUser.Name, idPartie=id))
            else:
                print("Aucune partie existante")

        if request.form['submit'] == 'Go Back':

            rmUserToDB(currentUser)
            return redirect(url_for("login"))

    return render_template('prepartie.html', name=name, UserList=users, GameidList=GameidList)


# acceuil et création d'un User
@app.route('/', methods=['POST', 'GET'])
def login():
    getUserList()
    validName = True
    Affiche = ""
    if request.method == 'POST':
        newName = request.form.get("name")
        if newName == "":
            validName = False
        for user in UserList:
            if user.Name == newName:
                validName = False
        if validName == True:
            CurrentUser = User(Name=(newName))
            addUserToDB(CurrentUser)
            if request.form['submit'] == 'Play':
                return redirect(url_for('prepartie', name=CurrentUser.Name))
        else:
            Affiche = "Username already used"
    return render_template('index.html', Affiche=Affiche)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=True)
