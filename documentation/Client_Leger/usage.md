# Documentation d'utilisation

Pour utiliser la solution proposée, suivez la documentation d'installation et appliques les règles suivantes.

Pour run le script de clonage et lancer le clone 

```bash
./clone.sh
```
Une fois log dans la VM enregistrez vos fichiers dans /mnt/Samba/USER afin qu'ils soient partagés sur le serveur.
Pour récupérer vos fichiers dans le serveur rendez vous dans le dossier /home/Share/VOTRE_USER.
Nb: le partage fonctionne à double sens; tous fichiers ajoutés ou supprimés dans l'une des destinations mentionés précédement seront appliqués automatiquement dans l'autre. 

# Fichier sauvegardés

Lors de son utilisation, le script créé une ligne de log dans le fichier situé /srv/backup/ClientLegerLog/LogScript

Le logiciel Samba a lui aussi des fichiers de logs situés par défault dans /var/log/samba.

Le lien suivant vous renverra vers le schema de notre solution.
https://gitlab.com/marieliserenzema/projet-infra/-/blob/main/documentation/shéma/diagram.pdf

