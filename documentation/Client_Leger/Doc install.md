# Projet Infra
# Documentation d'instalation de la solution de client leger
## Sommaire

- [Projet Infra](#projet-infra)
  - [Sommaire](#sommaire)
  - [Prérequis](#prérequis)
  - [1 Installation protocole RDP](#1-installation-protocole-rdp)
  - [2 Installation VirtualBox](#2-installation-virtualbox)
  - [3 Créer un nouvelle machine virtuelle](#3-créer-un-nouvelle-machine-virtuelle)
  - [4 Configuration Samba](#4-configuration-samba)
    - [Installation et conf de samba server](#installation-et-conf-de-samba-server)
    - [Installation et conf de samba client](#installation-et-conf-de-samba-client)
  - [5 script clonage VM](#5-script-clonage-vm)
 
### Prérequis 
-Un sreveur physique ou hébergé.
-Un os debian avec au moins 8Gi de RAM et un environnement de travail.

## 1 Installation protocole RDP 

Pour avoir un accès à notre environnement de travail, nous allons installer un server XRDP.  
```bash 
apt install xrdp 
systemctl status xrdp
```

XRDP ne peut être utilisé que par les utilisateur membres du groupe "ssl-cert" il faut donc ajouter l'uesr XRDP au groupe avec la commande suivante :
```bash 
adduser xrdp ssl-cert
```
Restart le service:
```bash
systemctl restart xrdp
```
Si vous utilisez un firewall il faut authoriser les connexions sur le port utilisé par XRDP.
Si vous ne l'avez pas changé, le port utilisé par XRDP est le port 3389
```bash 
ufw allow 3389
```
## 2 Installation VirtualBox

Pour la suite on vas installer Vbox et son extention VboxManage.
```bash
sudo apt install wget build-essential python2
```
```bash
wget https://download.virtualbox.org/virtualbox/6.1.24/VirtualBox-6.1.24-145767-Linux_amd64.run
```
On donne les droits pour utiliser le script d'installation.
```bash
chmod u+x VirtualBox-6.1.24-145767-Linux_amd64.run
```
On run le script d'installation 
```bash
sudo ./VirtualBox-6.1.24-145767-Linux_amd64.run
```

## 3 Créer un nouvelle machine virtuelle

Vous devez maintenant créer une nouvelle machine virtuelle en utilisant l'os de votre choix.

## 4 Configuration Samba

Samba est un logiciel qui permettra à l'utilisateur qui utilise la machine virtuelle de récuperer ses fichiers sauvegardé sur la machine virtuelle sur le server.

### Installation et conf de samba server
```bash 
sudo apt install samba
```

Création d'un partage samba.
Créez un repertoir dans le home 
```bash 
cd /home
sudo mkdir Share
```
Ensuite, créez dans ce repertoir des repertoir pour tout les utilisateurs potentiel de votre solution et attribuez le droit d'utiliser ce repertoir a l'utilisateur.
```bash
cd /Share 
sudo mkdir USER
sudo chown USER USER
```

Afin de créer et de configurer le partage, nous allons modifier le fichier smb.conf
```bash 
sudo nano /etc/samba/smb.conf
```
Rendez vous tout en bas du fichier et copiez-y les lignes suivantes en changeant les parties en maj par vos utilisateurs.  
```bash 
[NOM_Partage]
path = /home/Share/%u
read only = no
guest ok = no
force create mode = 0660
force directory mode = 2770
valid users = @sambashare
write list = [UTILISATEURS_DU_PARTAGE]
```
Redemarer le service 
```bash 
sudo systemctl restart smbd
```
Nous pouvons voir sur l'avant derniere ligne de la conf ci-dessus que la ligne "valid users" est sur @sambashare qui est un groupe d'utilisateurs qui regroupe tous les utilisateurs qui ont le droit d'utiliser votre solution.
Il faut donc ajouter vos utilisateurs à ce groupe avec la commande suivante :
```bash
sudo usermod -aG sambashare VOTRE_USER
```

Il faut ensuite configurer un password pour que l'utilisateur puisse acceder au partage.
```bash
sudo pdbedit -a VOTRE_USER
```
### Installation et conf de samba client

Pour installer samba en mode client, ouvrez la VM créé précédament.
Pour commancer, on vas faire en sorte que votre VM puisse communiquer avec votre server en utilisant son hostname.
```bash 
sudo nano /etc/hosts

Ajoutez la ligne suivante :
IP_DE_VOTRE_SREVER  VOTRE_HOSTNAME
```
Installation de samba 
```bash 
sudo apt install smbclient cifs-utils
```

Pour que samba monte automatiquement votre dossier partagé sur la VM modifiez dans le fichier /etc/fstab
```bash
sudo nano /etc/fstab

ajoutez la ligne suivante en modifiant VOTRE_HOSTNAME, USER et [X] un nombre associé a votre user.
//VOTRE_HOSTNAME/Share /mnt/Samba/USER cifs rw,uid=USER,gid=USER,credentials=/root/.smbcredentials[X],_netdev 0

créez le dossier dans la VM
cd 
cd /mnt
mkdir Samba
cd /Samba
mkdir USER 
```
credentials=/root/.smbcredentials seront des fichiers cachés qui permettent de stocker tous les users-name ainsi que leur mdp de mainere sécurisé de manière a ce que seulement l'utilisateur root puisse y accéder.
```bash
su -
mkdir ./smbcredentials[X]
Ajoutez-y les lignes suivantes
USERNAME=VOTRE_USER
PASSWORD=USER_PASSWORD
```
(NB:USER_PASSWORD doit etre le mdp configuré pour la conf du server samba (derniere commande de la partie 2)

Attribuez maintenant les droits minimal sur ces dossiers cachés
```bash
sudo chmod 600 /root/.smbcredentials[X]
```
## 5 script clonage VM

Pour cloner et lancer automatiquement la VM, créez un script.
```bash
cd /srv
sudo touch clone.sh 
sudo chmod +x clone.ch
```
Voici le script a copier dans le dossier 
```bash
user="$(whoami)
VM_name="Clone_for_$user"
Path=/VirtualBox\ VMs/clones/$VM_name

#Chech if user already have a VM
#Prevent the case ppl clone many VMs and fucked up your storage <3<3<3
if [ -d "$Path" ]; then 
    vboxmanage startvm "$VM_name" --type gui
    exit
fi
#clone and run masterVM
vboxmanage clonevm Db_patron --basefolder=/home/ed12/'VirtualBox VMs'/clones --register --name "$VM_name"
vboxmanage startvm "$VM_name" --type gui

#write log file for this script
date=$(date +"[%y/%/%d %H:%M:%S]")
log_file="/srv/backup/ClientLegerLog/LogScript"
log_line="${date} Virtual Machine was cloned by ${user} with the name ${VM_name}"
echo "${log_line}" >> "${log_file}
```






