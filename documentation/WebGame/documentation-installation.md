# Projet Infra

## Documentation d'installation.

## Sommaire

- [Projet Infra](#projet-infra)
  - [Sommaire](#sommaire)
  - [Intro](#intro-et-prérequis)
  - [Users](#users)
  - [WebServeur](#webserveur)
    - [Python3](#python3)
    - [Flask](#flask)
    - [Services](#services)
  - [NGINX](#nginx)
    - [Installation de NGINX](#installation-de-nginx)
    - [Installation de acme](#installation-de-acme)
    - [Configuration de NGINX](#configuration-de-nginx)
  - [MariaDB](#mariadb)
    - [Installation](#installation)
    - [Configuration](#configuration)
    - [Connector](#installation-de-mariaDB-connector )
  - [NFS](#nfs)
    - [Serveur](#serveur)
    - [Client](#client)
  - [NetData](#netdata)
    - [Installation](#installation)
    - [Configuration des alarmes sur discord](#configuration-des-alarmes-sur-discord)
    - [Exemple de configuration d'une alarme pour la RAM](#exemple-de-configuration-alarme-pour-la-ram)
  - [Shéma architecture serveur](#shéma-architecture-serveur)
  - [Conclusion](#conclusion)


## Intro et prérequis 

Ici vous trouverez le détail d'installation et de configuration du projet Infra.

Le projet à était mis en place sur un VPS OVH avec 2 Vcores, 4 GO de mémoire et 80 GO de stockage. La version de l'OS est une Debian 11.
Il est necessaire de possèder un nom de domaine. Pour ce document nous utiliseront alpagam.com 

## Users

Pour des raisons de sécurité nous allons débuter par la création deux utilisateurs applicatifs.

Le premier utilisateur sera l'administrateur des backup.
 ```bash
 paja444@SRVInfra:~$ adduser admin_backup
 ```
 Il fait parti du groupe "admins".
 ```bash
 paja444@SRVInfra:~$ usermod -aG sudo admin_backup
 ```
 Le second utilisateur sera l'administrateur de la partie serveur web.
  ```bash
 paja444@SRVInfra:~$ adduser admin_webserver
 ```
 Il fait parti du groupe "admins"
  ```bash
 paja444@SRVInfra:~$ usermod -aG sudo admin_webserver
 ```
 Modification du répertoire en /sbin/nologin
 ```bash
 paja444@SRVInfra:~$ sudo nano /etc/passwd
 ```
 ```bash
 admin_webserver:x:1004:1004:,,,:/home/admin_webserver:/usr/sbin/nologin
 admin_backup:x:1005:1005:,,,:/home/admin_backup:/usr/sbin/nologin
 ```

## WebServer
A présent nous allons mettre en place la partie serveur web.

### Python3
Installation de Python3.
```bash
paja444@SRVInfra:~$ sudo apt update & apt upgrade
paja444@SRVInfra:~$ sudo apt install wget build-essential libreadline-gplv2-dev libncursesw5-dev \
paja444@SRVInfra:~$ wget https://www.python.org/ftp/python/3.9.1/Python-3.9.1.tgz
paja444@SRVInfra:~$ tar xzf Python-3.9.1.tgz
paja444@SRVInfra:~$ cd Python-3.9.1
paja444@SRVInfra:~/Python-3.9.1$ /configure --enable-optimizations
paja444@SRVInfra:~$ make -j 2
paja444@SRVInfra:~$ sudo make alt install
```

### Flask
Installation du micro-framework Flask.
```bash
paja444@SRVInfra:~/projet-infra$ pip install Flask
```
Créez le dossier "~/projet-infra/Web_Game/" et ajoutez y les fichiers suivants:

📁 **Dossier** `~/projet-infra/Web_Game/`

[server.py](https://gitlab.com/-/ide/project/marieliserenzema/projet-infra/tree/main/-/Web_Game/server.py/) /
[server2.py](https://gitlab.com/-/ide/project/marieliserenzema/projet-infra/tree/main/-/Web_Game/server2.py/) /
[requirements.txt](https://gitlab.com/-/ide/project/marieliserenzema/projet-infra/tree/main/-/Web_Game/requirements.txt/) /
[static](https://gitlab.com/-/ide/project/marieliserenzema/projet-infra/tree/main/-/Web_Game/static/) /
[templates](https://gitlab.com/-/ide/project/marieliserenzema/projet-infra/tree/main/-/Web_Game/templates/) /

Vérifiez les permissions afin qu'elles soit comme ceci:
```bash
paja444@SRVInfra:~/projet-infra/Web_Game$ ls -al
-rw-r--r-- 1 admin_webserver admins     122 Feb 21 14:41 requirements.txt
-rw-r--r-- 1 admin_webserver admins   21641 Feb 21 14:41 server.py
-rw-r--r-- 1 admin_webserver admins   21641 Feb 21 15:00 server2.py
drwxr--r-- 2 admin_webserver admins    4096 Feb  8 16:29 static
drwxrwxr-- 2 admin_webserver admins    4096 Feb 17 15:07 templates
```
```bash
paja444@SRVInfra:~/projet-infra/Web_Game/templates$ ls -al
total 24
drwxrwxr-- 2 admin_webserver admins  4096 Feb 17 15:07 .
drwxr-xr-x 7 marielise       paja444 4096 Feb 21 15:00 ..
-rw-r--r-- 1 admin_webserver admins  1830 Feb 17 13:31 endGame.html
-rw-r--r-- 1 admin_webserver admins  1689 Feb 17 13:43 index.html
-rw-r--r-- 1 admin_webserver admins  3119 Feb 17 14:13 partie.html
-rw-r--r-- 1 admin_webserver admins  3439 Feb 17 15:07 prepartie.html
```
```bash
paja444@SRVInfra:~/projet-infra/Web_Game/static$ ls -al
total 1544
drwxr-xr-- 2 admin_webserver admins     4096 Feb  8 16:29 .
drwxr-xr-x 7 marielise       paja444    4096 Feb 21 15:00 ..
-rw-r--r-- 1 admin_webserver admins      623 Feb  8 16:29 10C.png
-rw-r--r-- 1 admin_webserver admins      554 Feb  8 16:29 10D.png
-rw-r--r-- 1 admin_webserver admins      630 Feb  8 16:29 10H.png
-rw-r--r-- 1 admin_webserver admins      596 Feb  8 16:29 10S.png
-rw-r--r-- 1 admin_webserver admins      457 Feb  8 16:29 2C.png
-rw-r--r-- 1 admin_webserver admins      411 Feb  8 16:29 2D.png
-rw-r--r-- 1 admin_webserver admins      474 Feb  8 16:29 2H.png
-rw-r--r-- 1 admin_webserver admins      463 Feb  8 16:29 2S.png
-rw-r--r-- 1 admin_webserver admins      505 Feb  8 16:29 3C.png
-rw-r--r-- 1 admin_webserver admins      440 Feb  8 16:29 3D.png
-rw-r--r-- 1 admin_webserver admins      508 Feb  8 16:29 3H.png
-rw-r--r-- 1 admin_webserver admins      511 Feb  8 16:29 3S.png
-rw-r--r-- 1 admin_webserver admins      474 Feb  8 16:29 4C.png
-rw-r--r-- 1 admin_webserver admins      435 Feb  8 16:29 4D.png
-rw-r--r-- 1 admin_webserver admins      494 Feb  8 16:29 4H.png
-rw-r--r-- 1 admin_webserver admins      486 Feb  8 16:29 4S.png
-rw-r--r-- 1 admin_webserver admins      553 Feb  8 16:29 5C.png
-rw-r--r-- 1 admin_webserver admins      478 Feb  8 16:29 5D.png
-rw-r--r-- 1 admin_webserver admins      578 Feb  8 16:29 5H.png
-rw-r--r-- 1 admin_webserver admins      558 Feb  8 16:29 5S.png
-rw-r--r-- 1 admin_webserver admins      510 Feb  8 16:29 6C.png
-rw-r--r-- 1 admin_webserver admins      463 Feb  8 16:29 6D.png
-rw-r--r-- 1 admin_webserver admins      531 Feb  8 16:29 6H.png
-rw-r--r-- 1 admin_webserver admins      520 Feb  8 16:29 6S.png
-rw-r--r-- 1 admin_webserver admins      610 Feb  8 16:29 7C.png
-rw-r--r-- 1 admin_webserver admins      513 Feb  8 16:29 7D.png
-rw-r--r-- 1 admin_webserver admins      606 Feb  8 16:29 7H.png
-rw-r--r-- 1 admin_webserver admins      609 Feb  8 16:29 7S.png
-rw-r--r-- 1 admin_webserver admins      612 Feb  8 16:29 8C.png
-rw-r--r-- 1 admin_webserver admins      525 Feb  8 16:29 8D.png
-rw-r--r-- 1 admin_webserver admins      625 Feb  8 16:29 8H.png
-rw-r--r-- 1 admin_webserver admins      618 Feb  8 16:29 8S.png
-rw-r--r-- 1 admin_webserver admins      679 Feb  8 16:29 9C.png
-rw-r--r-- 1 admin_webserver admins      533 Feb  8 16:29 9D.png
-rw-r--r-- 1 admin_webserver admins      634 Feb  8 16:29 9H.png
-rw-r--r-- 1 admin_webserver admins      608 Feb  8 16:29 9S.png
-rw-r--r-- 1 admin_webserver admins      440 Feb  8 16:29 AC.png
-rw-r--r-- 1 admin_webserver admins      388 Feb  8 16:29 AD.PNG
-rw-r--r-- 1 admin_webserver admins      453 Feb  8 16:29 AH.png
-rw-r--r-- 1 admin_webserver admins      583 Feb  8 16:29 AS.png
-rw-r--r-- 1 admin_webserver admins     3153 Feb  8 16:29 JC.png
-rw-r--r-- 1 admin_webserver admins     3046 Feb  8 16:29 JD.png
-rw-r--r-- 1 admin_webserver admins     3111 Feb  8 16:29 JH.png
-rw-r--r-- 1 admin_webserver admins     3092 Feb  8 16:29 JS.png
-rw-r--r-- 1 admin_webserver admins     3033 Feb  8 16:29 KC.png
-rw-r--r-- 1 admin_webserver admins     3093 Feb  8 16:29 KD.png
-rw-r--r-- 1 admin_webserver admins     3189 Feb  8 16:29 KH.png
-rw-r--r-- 1 admin_webserver admins     3083 Feb  8 16:29 KS.png
-rw-r--r-- 1 admin_webserver admins     3044 Feb  8 16:29 QC.png
-rw-r--r-- 1 admin_webserver admins     3110 Feb  8 16:29 QD.png
-rw-r--r-- 1 admin_webserver admins     3149 Feb  8 16:29 QH.png
-rw-r--r-- 1 admin_webserver admins     3220 Feb  8 16:29 QS.png
-rw-r--r-- 1 admin_webserver admins      239 Feb  8 16:29 back.png
-rw-r--r-- 1 admin_webserver admins  1352753 Feb  8 16:29 table.jpg
```
### Services 
Mise en place des services qui serviront le serveur NGINX.
```bash
paja444@SRVInfra:~$ sudo nano /etc/systemd/system/batailleWeb.service
```
```bash
[Unit]
Description=serve BatailleWeb
After=network.target

[Service]
User=admin_webserver
Group=www-data
WorkingDirectory=/home/paja444/projet-infra/Web_Game
ExecStart=python3 server.py
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
```bash
paja444@SRVInfra:~$ sudo nano /etc/systemd/system/batailleWeb2.service
```
```bash
[Unit]
Description=serve BatailleWeb-2
After=network.target

[Service]
User=admin_webserver
Group=www-data
WorkingDirectory=/home/paja444/projet-infra/Web_Game
ExecStart=python3 server2.py
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
```bash
paja444@SRVInfra:~$ sudo systemctl daemon-reload
```
Les permissions doivent être comme celle-ci:
```bash
paja444@SRVInfra:/etc/systemd/system$ ls -al
total 80
drwxr-xr-x 15 root            root   4096 Feb 20 23:58 .
drwxr-xr-x  5 root            root   4096 Sep 28 04:32 ..
-rw-r-----  1 admin_backup    admins  159 Feb 20 23:57 backup.service
-rw-r-----  1 admin_backup    admins  169 Feb 17 13:03 backup.timer
-rw-r-----  1 admin_webserver admins  290 Feb 20 23:52 batailleWeb.service
-rw-r-----  1 admin_webserver admins  294 Feb 20 23:53 batailleWeb2.service
```

### NGINX
Maintenant nous allons installer et configurer NGINX afin qu'il serve les deux serveurs web Python "batailleWeb.service" et "batailleWeb2.service"

#### Installation de NGINX
```bash
paja444@SRVInfra:~$ sudo apt install nginx
```
#### Installation de acme
Afin d'obtenir un certificat "ssl" nous allons installer "acme.sh".
```bash
paja444@SRVInfra:~$ git clone https://github.com/acmesh-official/acme.sh.git
paja444@SRVInfra:~$ cd ./acme.sh
paja444@SRVInfra:~./acme.sh$ ./acme.sh --install -m my@example.com
```
Générer le certificat:
```bash
paja444@SRVInfra:~$ acme.sh --issue -d alpagam.com -w /home/wwwroot/alpagam.com
```
Redémarrer le serveur.
```bash
paja444@SRVInfra:~$ sudo reboot
```

#### Configuration de NGINX

```bash
paja444@SRVInfra:~$ sudo ufw allow 'Nginx Full'
```
```bash
paja444@SRVInfra:~$ sudo nano /etc/nginx/conf.d/batailleWeb.conf
```

Configuration du revers proxy NGINX, de la répartition de charges, du certificat ssl, nom de domaine et redirection http/https.
```bash
upstream backend_flask{
  server 127.0.0.1:5000;
  server 127.0.0.1:5001;
}

server {
    listen 80;
    return 301 https://$host$request_uri;
}

server {
  listen 443 ssl;
  ssl_certificate /root/.acme.sh/alpagam.com/alpagam.com.cer;
  ssl_certificate_key /root/.acme.sh/alpagam.com/alpagam.com.key;
  server_name www.alpagam.com alpagam.com;

  location / {
    include proxy_params;
    proxy_pass http://backend_flask;
  }
}

```

### MariaDB
#### Installation
Installation de Mariadb serveur et client.
```bash
paja444@SRVInfra:~$ sudo apt-get install curl software-properties-common dirmngr
```
```bash
paja444@SRVInfra:~$ curl -LsS -O https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
paja444@SRVInfra:~$ sudo bash mariadb_repo_setup --os-type=debian  --os-version=buster --mariadb-server-version=10.6
paja444@SRVInfra:~$ wget http://ftp.us.debian.org/debian/pool/main/r/readline5/libreadline5_5.2+dfsg-3+b13_amd64.deb
paja444@SRVInfra:~$ sudo dpkg -i libreadline5_5.2+dfsg-3+b13_amd64.deb
```
```bash
paja444@SRVInfra:~$ sudo apt-get update
paja444@SRVInfra:~$ sudo apt-get install mariadb-server mariadb-client
```
```bash
paja444@SRVInfra:~$ sudo systemctl start mariadb
paja444@SRVInfra:~$ sudo systemctl enable mariadb
```
```bash
paja444@SRVInfra: sudo mysql_secure_installation
```
```bash
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] Y
Enabled successfully!
Reloading privilege tables..
 ... Success!


You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] Y
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

####configuration:
```bash
paja444@SRVInfra:~$ sudo ufw allow 3306
```
```bash
paja444@SRVInfra:~$ mysql -u root -p
```
Création de le base de données.
```bash
MariaDB [(none)]> CREATE DATABASE webGameDB ;
```
```bash
MariaDB [(none)]> use webGameDB              
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [webGameDB]> 
```
Création de la table "Users".
```bash
CREATE TABLE Users (
id MEDIUMINT NOT NULL AUTO_INCREMENT,
userData JSON,
);
```
Création de la table "Games".
```bash
CREATE TABLE Games (
id MEDIUMINT NOT NULL AUTO_INCREMENT,
gameData JSON,
);
```
Création des utilisateurs de la base.

user admin pour la gestion.
```bash
CREATE USER 'admin'@'54.37.153.179' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON webGameDB.* TO 'admin'@'54.37.153.179';
FLUSH PRIVILEGES;
```
user "server1" sera utilisé pour lier le premier serveur Python Flask à la base de données.
```bash
CREATE USER 'server1'@'54.37.153.179' IDENTIFIED BY 'server1';
GRANT ALL PRIVILEGES ON webGameDB.* TO 'server1'@'54.37.153.179';
FLUSH PRIVILEGES;
```
user "server2" sera utilisé pour lier le second serveur Python Flask à la base de données.
```bash
CREATE USER 'server2'@'54.37.153.179' IDENTIFIED BY 'server2';
GRANT ALL PRIVILEGES ON webGameDB.* TO 'server2'@'54.37.153.179';
FLUSH PRIVILEGES;
```

#### Installation de mariaDB connector

MariaDB Connector/Python est une api permettant la communication entre un programme Python et la base de données Mariadb.

```bash
paja444@SRVInfra:~$ pip3 install mariadb
```

# NFS
Nous avons mis en place une server NFS afin de pouvoir effecuer des sauvegardes quotidienne du projet. Pour des raisons financières nous n'avons pas loué un second serveur OVH malgrés que ce serai pertinant de stocker nos sauvegarde sur une autre machine. Tout comme MariaDB, le client et le serveur seront alors sur la même machine pour la démonstration technique.

## NFS serveur

### Installation
```bash
paja444@SRVInfra:~$ apt install nfs-kernel-server
```
#### Configuration NFS serveur
```bash
paja444@SRVInfra:~$ sudo ufw allow 2049
```

```bash
paja444@SRVInfra:~$ sudo nano /etc/idmapd.conf
```
```bash
[General]

#Verbosity = 0
#Pipefs-Directory = /run/rpc_pipefs
# set your own domain here, if it differs from FQDN minus hostname
 Domain = projetInfra

[Mapping]

Nobody-User = nobody
Nobody-Group = nogroup
```
```bash
paja444@SRVInfra:~$ sudo nano /etc/exports
```
```bash
# /etc/exports: the access control list for filesystems which may be exported
#               to NFS clients.  See exports(5).
#
# Example for NFSv2 and NFSv3:
# /srv/homes       hostname1(rw,sync,no_subtree_check) hostname2(ro,sync,no_subtree_check)
#
# Example for NFSv4:
# /srv/nfs4        gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
# /srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)
#
/mnt/backup 54.37.153.179(rw,no_root_squash)
```
## NFS client

### Installation
```bash
paja444@SRVInfra:~$ apt install nfs-common
```
#### Configuration NFS client

```bash
paja444@SRVInfra:~$ mkdir /srv/backup
```
```bash
paja444@SRVInfra:~$ sudo mount -t nfs 54.37.153.179:/mnt/backup/ /srv/backup
```
```bash
# /etc/fstab: static file system information
UUID=a50e41bb-be24-4830-ac0c-2628a19c55a7 / ext4 rw,discard,errors=remount-ro,x-systemd.growfs 0 1
UUID=A3A9-FBAB /boot/efi vfat defaults 0 0
54.37.153.179:/mnt/backup/ /srv/backup nfs defaults 0 0
```
# NetData

### Installation

```bash
marielise@SRVInfra:~$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)
marielise@SRVInfra:~$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/lib/systemd/system/netdata.service; enabled; vendor prese>
     Active: active (running) since Mon 2022-02-21 11:34:49 UTC; 56s ago
```

Pour accéder au siteWeb associé 

```bash
marielise@SRVInfra:~$ curl http://54.37.153.179:19999/
```

#### Configuration des alarmes sur discord 

```bash 
marielise@SRVInfra:~$ cd /etc/netdata
marielise@SRVInfra:/etc/netdata$ sudo ./edit-config health_alarm_notify.conf
Editing '/etc/netdata/health_alarm_notify.conf' ...
```
Les modifications dans le fichier health_alarm_notify.conf

```bash 
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/940626800688627752/SXCG_M>

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

#### Exemple de configuration alarme pour la RAM

```bash
marielise@SRVInfra:~$ cd /etc/netdata/health.d
marielise@SRVInfra:~$ sudo ./edit-config health.d/ram.conf
```
Les modifications dans le fichier ram.conf

```bash 
alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 40
  crit: $this > 60
  info: The percentage of RAM being used by the system.
```

```bash
marielise@SRVInfra:~$ netdatacli reload-health
```
## Shéma architecture serveur

https://gitlab.com/-/ide/project/marieliserenzema/projet-infra/tree/main/-/documentation/shéma/shema-architecture-server.png/

## Conclusion

Pour résumer nous avons deux [serveurs web Python Flask](#flask) qui tournent dans les services "[bataille Web 1.service](#services)" et "[bataille Web1.service](#services)". Ces deux services servent un serveur ["revers proxy" NGINX](#configuration-de-nginx) qui permet le passage en HTTPS ainsi que la répartition se chargent et distribuent les requêtes des clients vers le serveur 1 et le serveur 2. Afin que les données soient communes aux deux serveurs nous avons mis en place une [base de données Maria Db](#configuration). Elle communique directement avec les serveurs Python Falsk grace à ["MariaDB connector"](#installation-de-mariaDB-connector). D'autre part à fin de suivre l'évolution des ressources du serveur OVH nous avons mis en place l'application de "monitoring"[NetData](#netdata). Pour finir, nous avons configuré un service de [backup](../backup/documentation-backup.md) qui sauvegarde quotidiennement notre solution et l'exporte grace au serveur [NFS](#nfs).    
