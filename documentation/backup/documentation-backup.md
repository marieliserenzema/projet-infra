# Projet Infra

## Documentation backup.

## Sommaire

- [Projet Infra](#projet-infra)
  - [Sommaire](#sommaire)
  - [Intro](#intro-et-prérequis)
  - [backup base de donnée](#backup-base-de-donnée)
  - [backup fichier de configuration](#backup-fichier-de-configuration)
  - [backup Logs](#backup-logs)
  - [backup webGame](#backup-webgame)
  - [backup service](#backup-service)
  - [Conclusion](#conclusion)

## Intro-et-prérequis

L'objet de cette partie est de mettre en place une sauvegarde quotidienne de l'ensemble du projet afin d'obtenir une résilience face aux différents imprévus qui peuvent compromettre son bon fonctionnement.
Il est nécessaire d'avoir préalablement installé et configurer le serveur [NFS](../WebGame/documentation-installation.md) ainsi que [MariaDB](../WebGame/documentation-installation.md).

## Backup base de donnée
Pour la base de données nous allons faire un "dump".

Préparation d'un fichier seulement visible par le user admin_backup:
```bash
paja444@SRVInfra:~$ sudo -u admin_backup nano /.my.cnf
```
```bash
user=admin
passwd=admin
```
Fichier de log.
```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /var/log/backup/webGameDB/
```
Fichier de sauvegarde.
```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /srv/backup/webGameDB/
```

Création de script de dump de la base mysql:
```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir script
```
```bash
paja444@SRVInfra:~/script$ sudo -u admin_backup nano backupDBWebgame.sh
```
```bash
# Save DB Data.
# paja444

# Script vars
log_date=$(date +"[%y/%m/%d %H:%M:%S]")
log_dir='/var/log/backup/webGameDB/'
log_file="${log_dir}/backup_db.log"

name_date=$(date +"%y%m%d")
name_time=$(date +"%H%M%S")
file_name="WebGame_db${name_date}_${name_time}"
save_dir='/srv/backup/webGameDB/'
save_file=${save_dir}${file_name}
user=$(cat /.my.cnf | grep "user" | cut -d '=' -f2)
passwd=$(cat /.my.cnf | grep "passwd" | cut -d '=' -f2)

# Preflight checks
if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

if [[ ! -w "${save_dir}" ]]
then
  echo "Save dir ${log_dir} is not writable."
  exit 1
fi


#Save WebGame_db data
mkdir ${save_file}
mysqldump -h 127.0.0.1 -p${passwd} -u${user} -P 3306 webGameDB > dump.sql
mv dump.sql ${save_file}
tar -czf ${save_file}.tar.gz ${save_file} 2> /dev/null
rm -r ${save_file}
# Write in logfile
log_line="${log_date} Backup ${save_dir}${file_name} created successfully."
echo "${log_line}" >> "${log_file}"

#Print
echo "Backup ${save_dir}${file_name} created successfully."
```

## backup fichier de configuration

Pour la sauvegarde des fichiers de conf nous allons faire une copie des fichiers:
  - conf NGINX : /etc/nginx/conf.d/batailleWeb.conf
  - conf GameServer1 : /etc/systemd/system/batailleWeb.service
  - conf GameServer2 : /etc/systemd/system/batailleWeb2.service
  - conf Samba : /etc/samba/smb.conf

Créer le dossier "serverConf/" dans /var/log
```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /var/log/backup/serverConf/
```
Créer le dossier "serverConf/" dans /srv/backup
```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /srv/backup/serverConf/
```
Création du script:
```bash
paja444@SRVInfra:~/script$ sudo -u admin_backup nano backupConfWebGame.sh
```
```bash
# Save OVH server conf.
# paja444

# Script vars
log_date=$(date +"[%y/%m/%d %H:%M:%S]")
log_dir='/var/log/backup/serverConf/'
log_file="${log_dir}/serverConf.log"

name_date=$(date +"%y%m%d")
name_time=$(date +"%H%M%S")
file_name="serverConf:${name_date}_${name_time}"
save_dir='/srv/backup/serverConf/'
save_file=${save_dir}${file_name}

nginx_conf="/etc/nginx/conf.d/batailleWeb.conf"
GameServer1_conf="/etc/systemd/system/batailleWeb.service"
GameServer2_conf="/etc/systemd/system/batailleWeb2.service"
Samba_conf="/etc/samba/smb.conf"
# Preflight checks
if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

if [[ ! -w "${save_dir}" ]]
then
  echo "Save dir ${log_dir} is not writable."
  exit 1
fi

#Save OVH conf
mkdir ${save_file}
cp -r ${nginx_conf} ${save_file}
cp -r ${GameServer1_conf} ${save_file}
cp -r ${GameServer2_conf} ${save_file} 
cp -r ${Samba_conf} ${save_file}

# Write in logfile
log_line="${log_date} Backup ${save_dir}${file_name} created successfully."
echo "${log_line}" >> "${log_file}"

#Print
echo "Backup ${save_dir}${file_name} created successfully."
```
## Backup Logs

Pour les Logs nous allont sauvegarder:
  - log Samba : /var/log/samba/log.smbd
  - log batailleWeb.service : journalctl -u batailleWeb.service
  - log batailleWeb2.service : journalctl -u batailleWeb2.service

```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /srv/backup/WebGameServerLog/
```
```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /var/log/backup/WebGameServerLog/
```
```bash
paja444@SRVInfra:~/script$ sudo mkdir /srv/backup/SambaLog/
```
```bash
paja444@SRVInfra:~/script$ sudo nano backupLogWebGameServer.sh
```
```bash
name_date=$(date +"%y%m%d")
name_time=$(date +"%H%M%S")
file_name="webGameLogServer${name_date}_${name_time}"
file_name_samba="sambaLog${name_date}_${name_time}"
log_dir='/var/log/backup/WebGameServerLog/'
save_dir='/srv/backup/WebGameServerLog/'
save_dir_samba="/srv/backup/SambaLog/"
save_file=${save_dir}${file_name}
save_file_samba=${save_dir_samba}${file_name_samba}

samba_log="/var/log/samba/log.smbd"

# Preflight checks
if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

if [[ ! -w "${save_dir}" ]]
then
  echo "Save dir ${log_dir} is not writable."
  exit 1
fi


#Save WebGameServer Log data
rm -r ${save_dir}
mkdir ${save_dir}
mkdir ${save_file}
mkdir ${save_file_samba}
journalctl -u batailleWeb.service > server1Journal
journalctl -u batailleWeb2.service > server2Journal
mv server1Journal ${save_file}
mv server2Journal ${save_file}
cp ${samba_log} ${save_file_samba}

# Write in logfile
log_line="${log_date} Backup ${save_dir}${file_name} created successfully."
echo "${log_line}" >> "${log_file}"
log_line_samba="${log_date} Backup ${save_dir_samba}${file_name_samba} created successfully."
echo "${log_line_samba}" >> "${log_file_samba}"
#Print
echo "Backup ${save_dir}${file_name} created successfully."
echo "Backup ${save_dir_samba}${file_name_samba} created successfully."
```
## Backup WebGame

Ici nous allons sauvegarder le fichier /projet-infra contenant l'ensemble du projet. Le dossier de Client Léger, Les serveur Flask, les templates, les images etc..

```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /var/log/backup/ProjetinfraFiles/
```
```bash
paja444@SRVInfra:~$ sudo -u admin_backup mkdir /srv/backup/ProjetinfraFiles/
```
```bash
paja444@SRVInfra:~/script$ sudo -u admin_backup nano backupProjectInfra.sh 
```
```bash
# Save ProjectFiles.
# paja444

# Script vars
log_date=$(date +"[%y/%m/%d %H:%M:%S]")
log_dir='/var/log/backup/ProjetinfraFiles/'
log_file="${log_dir}/ProjetinfraFiles.log"

name_date=$(date +"%y%m%d")
name_time=$(date +"%H%M%S")
file_name="ProjetinfraFiles:${name_date}_${name_time}"
save_dir='/srv/backup/ProjetinfraFiles/'
save_file=${save_dir}${file_name}
project_path="/home/paja444/projet-infra"
# Preflight checks
if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

if [[ ! -w "${save_dir}" ]]
then
  echo "Save dir ${log_dir} is not writable."
  exit 1
fi

if [[ ! -w "${project_path}" ]]
then
  echo "${project_path} file does not exist."
  exit 1
fi

#Save WebGame_db data
mkdir ${save_file}
cp -r ${project_path} ${save_file} 
tar -czf ${save_file}.tar.gz ${save_file} 2> /dev/null
rm -r ${save_file}
# Write in logfile
log_line="${log_date} Backup ${save_dir}${file_name} created successfully."
echo "${log_line}" >> "${log_file}"

#Print
echo "Backup ${save_dir}${file_name} created successfully."
```
## Backup Service

Attribuez le groupe "admins", propriétaire "admin_backup" et permmission rwx xr pour tout les scripts.

Permission:
```bash
paja444@SRVInfra:~/script$ sudo chmod o-x backupProjectInfra.sh
```
groupe:
```bash
paja444@SRVInfra:~/script$ sudo chgrp admins backupLogWebGameServer.sh
```
propriétaire:
```bash
paja444@SRVInfra:~/script$ sudo chown admin_backup backupLogWebGameServer.sh
```
Vous devez obtenir ces permissions et User/groupe:
```bash
paja444@SRVInfra:~/script$ ls -al
total 28
drwxr-xr-x  2 admin_backup admins  4096 Feb 20 23:38 .
drwxr-xr-x 24 paja444      paja444 4096 Feb 20 22:05 ..
-rwxr-xr--  1 admin_backup admins   784 Feb 15 16:16 backup.sh
-rwxr-xr--  1 admin_backup admins  1120 Feb 16 08:04 backupConfWebGame.sh
-rwxr-xr--  1 admin_backup admins  1051 Feb 15 11:38 backupDBWebgame.sh
-rwxr-xr--  1 admin_backup admins  1596 Feb 16 08:08 backupLogWebGameServer.sh
-rwxr-xr--  1 admin_backup admins  1049 Feb 15 12:08 backupProjectInfra.sh
a présent nous allons créer un service backup.service qui lancera les scripts de backup quotidiennement.
```

```bash
paja444@SRVInfra:~/script$ sudo -u admin_backup nano backup.sh
```
```bash
#Launch Backup ProjetInfra.
# paja444

# Script vars
log_date=$(date +"[%y/%m/%d %H:%M:%S]")
log_dir='/var/log/backup/'
log_file="${log_dir}/backupProjetInfra.log"

name_date=$(date +"%y%m%d")
name_time=$(date +"%H%M%S")

# Preflight checks
if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

#Save WebGame_db data & BatailleWebServer1 & BatailleWebServer1 journals
bash /home/paja444/script/backupDBWebgame.sh
bash /home/paja444/script/backupLogWebGameServer.sh
bash /home/paja444/script/backupProjectInfra.sh
bash /home/paja444/script/backupConfWebGame.sh
# Write in logfile
log_line="${log_date} Backup ProjetInfra created successfully."
echo "${log_line}" >> "${log_file}"

#Print
echo "${log_date} Backup ProjetInfra created successfully."
```

#### création du service:

```bash
paja444@SRVInfra:~$ sudo nano /etc/systemd/system/backup.service 
```
```bash
[Unit]
Description="Backup"

[Service]
User=admin_backup   
ExecStart=sudo bash /home/paja444/script/backup.sh
Type=oneshot

[Install]
WantedBy=multi-user.target
```

```bash
paja444@SRVInfra:~$ sudo nano /etc/systemd/system/backup.timer 
```
```bash
[Unit]
Description=Lance backup.service a interrvalles reguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=daily

[Install]
WantedBy=timers.target
```

```bash
paja444@SRVInfra:~$ sudo systemctl daemon-reload
```
## Conclusion

Pour résumer nous avons créer un script "[backup.sh](#backup-service)" qui est lancé par le service "[backup.service](#backup-service)" selont les ordres de "[backup.timer](#création-du-service)" afin de lancer nos quatres scripts de sauvegardes: [backup base de données](#backup-base-de-donnée); [backup fichier de configuration](#backup-fichier-de-configuration); [backup Logs](#backup-logs) et [backup webGame](#backup-webgame)
